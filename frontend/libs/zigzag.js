
///////////
// Utils //
///////////

// Formats a given date into: [MONTH] [YEAR]
function dateStr(d) {
  var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  return `${months[d.getMonth()]} ${d.getFullYear()}`;
}



/////////////////////////////
/// Zigs, Zags, & SubZags ///
/////////////////////////////

// A Zig is a markdown document with json meta-data
function Zig(json, zag) {
  // Init
  this.meta = json['meta'];
  this.md = json['md'];
  this.html = marked.parse(json['md']);
  this.zag = zag;

  // Render the element for this Zig
  this.render = () => {
    var e = $('<div>').addClass('zig');
    var meta = $('<div>').addClass('zig-meta');
    var tags = this.meta['tags'].map(t => $('<a>').attr('href', '#?%' + t).html(t));
    meta.append($('<span>').addClass('date').html(dateStr(new Date(this.meta['date'] * 1000))));
    meta.append($('<span>').addClass('tags').html(tags));
    if (this.meta['title'] != undefined) meta.append($('<h1>').addClass('title').html(this.meta['title']));
    e.append(meta);
    e.append(this.html);
    e.html(this.renderSubQueries(e.html()));
    return e;
  }

  // Renders & embeds subqueries of a Zig
  this.renderSubQueries = html => {
    html = html.replace(/#{\s*([^}]*)\s*}/, (m, q) => {
      var list = $('<div>').addClass('zig-list');
      this.zag.q(q).zigs.map(z => {
        var i = $('<div>').addClass('zig-list-item');
        var t = z.meta['title'] != undefined ? z.meta['title'] : z.meta['name'];
        i.append($('<span>').addClass('date').html(dateStr(new Date(z.meta['date'] * 1000))));
        i.append($('<a>').attr('href', '#?' + z.meta['name']).addClass('title').html(t));
        list.append(i);
      });
      return list.prop('outerHTML');
    });
    return html;
  }

  // Compare Zigs with date meta-data
  this.date_cmp = (z) => this.meta['date'] < z.meta['date'];
}


// A Zag is a collection of Zigs
function Zag(json) {
  // Internally Zigs are stored sorted by date for faster query operations
  this.zigs = json.map(z => new Zig(z, this)).sort((x, y) => x.date_cmp(y));

  // The trivial query for all Zigs in this Zag
  this.q_all = () => new SubZag(this.zigs);

  // A query for all Zigs where property k=v
  this.q_eq = (k, v) => new SubZag(this.zigs.filter(z => z.meta[k] == v));

  // A query for all Zigs where property k contains value v
  this.q_has = (k, v) => new SubZag(this.zigs.filter(z => z.meta[k].includes(v)));

  // A query for all Zigs with a given name
  this.q_name = name => this.q_eq('name', name);

  // A query for all Zigs with a given tag
  this.q_tag = tag => this.q_has('tags', tag);

  // Run a query command
  this.q = cmd => new QueryOp(cmd).eval(this);
}


// A SubZag represents a subset of a Zag produced by a query
function SubZag(zigs) {
  // Init
  this.zigs = zigs;

  // Render all Zigs in the SubZag
  this.render = () => this.zigs.map(z => z.render());

  // An intersection operation on SubZags
  this.q_and = (q) => {
    var q1 = this.zigs; var q2 = q.zigs; var i = 0; var j = 0; var l = [];
    while(i < q1.length && j < q2.length) {
      if (q1[i] == q2[j]) { l.push(q1[i++]); j++; continue }
      if (q1[i].date_cmp(g2[j])) { i++ }
      else { j++ }
    }
    new SubZag(l);
  };

  // A union operation on SubZags
  this.q_or = (q) => {
    var q1 = this.zigs; var q2 = q.zigs; var i = 0; var j = 0; var l = [];
    while(i < q1.length || j < q2.length) {
      if (i >= q1.length) { l.push(q2[j++]); continue }
      if (j >= q2.length) { l.push(q1[i++]); continue }
      if (q1[i] == q2[j]) { l.push(q1[i++]); j++; continue }
      if (q1[i].date_cmp(g2[j])) { l.push(q1[i++]); }
      else { l.push(q2[j++]); }
    }
    new SubZag(l);
  };

  // A difference operation on SubZags
  this.q_sub = (q) => {
    var q1 = this.zigs; var q2 = q.zigs; var i = 0; var j = 0; var l = [];
    while(i < q1.length) {
      if (j >= q2.length) { l.push(q1[i++]); continue }
      if (q1[i] == q2[j]) { i++; j++; continue }
      if (q1[i].date_cmp(g2[j])) { l.push(q1[i++]); }
      else { j++; }
    }
    new SubZag(l);
  };
}



//////////////////////////
/// Query Language AST ///
//////////////////////////

/*
GRAMMAR:

  QUERY-OP = QUERY-ATOM | AND | OR | SUB
  AND = QUERY-ATOM , QUERY-OP
  OR = QUERY-ATOM ; QUERY-OP
  SUB = QUERY-ATOM / QUERY-OP
  QUERY-ATOM = ALL | EQ | HAS | TAG | NAME
  ALL = @
  EQ = LITERAL : LITERAL
  HAS = LITERAL . LITERAL
  TAG = % LITERAL
  NAME = LITERAL
  LITERAL= /[^,;/@:.%]+/

*/


// Binary operations between atomic queries
function QueryOp(cmd) {
  // Parse & Init
  var m = cmd.match(/^([^,;/]+)([,;/])?/);
  if (m[2] == undefined) return new QueryAtom(m[1]);
  this.lhs = new QueryAtom(m[1]);
  this.op = {',':'and',';':'or','/':'sub'}[m[2]];
  this.rhs = new QueryOp(cmd.slice(m[0].length));

  // Evaluate binary query operation
  this.eval = (zag) => {
    var lhs_q = this.lhs.eval(zag);
    var rhs_q = this.rhs.eval(zag);
    switch (this.op) {
      case 'and': return lhs_q.q_and(rhs_q);
      case 'or': return lhs_q.q_or(rhs_q);
      case 'sub': return lhs_q.q_sub(rhs_q);
    }
  }
}

// Atomic queries
function QueryAtom(cmd) {
  // Parse & Init
  var m;
  if (m=cmd.match(/^@/)) { this.type='all'; }
  if (m=cmd.match(/^%([^,;@%:.]+)$/)) { this.type='tag'; this.k=m[1]; }
  if (m=cmd.match(/^([^,;/@%:.]+)$/)) { this.type='name'; this.k=m[1]; }
  if (m=cmd.match(/^([^,;@%:.]+):([^,;@%:.]+)$/)) { this.type='eq'; this.k=m[1]; this.v=m[2]; }
  if (m=cmd.match(/^([^,;@%:.]+)\.([^,;@%:.]+)$/)) { this.type='has'; this.k=m[1]; this.v=m[2]; }

  // Evaluate an atomic query
  this.eval = (zag) => {
    switch (this.type) {
      case '@': return zag.q_all();
      case 'tag': return zag.q_tag(this.k);
      case 'name': return zag.q_name(this.k);
      case 'eq': return zag.q_eq(this.k, this.v);
      case 'has': return zag.q_has(this.k, this.v);
      default: return new SubZag([]);
    }
  }
}



/////////////////
/// Interface ///
/////////////////

// Initialize main Zag & top level query functions
function initZZ() {
  // Main Zag
  window.Z = new Zag(JSON.parse($('#zag').html()));

  // Run query on main Zag
  window.Q = query => window.Z.q(query);

  // Show the given query in the main content div
  window.SQ = query => $('#content').html(window.Q(query).render());

  // Renders a query in a URL variables field
  window.VQ = () => {
    var q = window.location.search.substring(1);
    if (q != '') window.SQ(q);
  }

  // Renders a query in a URL hash
  window.HQ = () => {
    var h = window.location.hash.substring(1);
    if (h[0] == '?') window.SQ(h.substring(1));
  }
}
