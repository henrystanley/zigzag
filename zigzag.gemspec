Gem::Specification.new do |s|
  s.name        = 'zigzag'
  s.version     = '0.0.0'
  s.date        = '2020-03-02'
  s.summary     = 'ZigZag Static Site Generator'
  s.description = 'A minimalist SPA Static Site Generator'
  s.authors     = ["Nicholas Rakita"]
  s.email       = 'kolyadr@gmail.com'
  s.files       = ["lib/zigzag.rb"] + Dir['frontend/*'] + Dir['frontend/*/*']
  s.homepage    = 'https://rubygems.org/gems/zigzag'
  s.license     = 'Unlicense'
  s.executables << 'zz'
end