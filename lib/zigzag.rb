#!/usr/bin/ruby

####### ZigZag ########
## A Document System ##
#######################

require 'json'
require 'digest'
require 'nokogiri'

module ZigZag

  # A Zig is a markdown document with additional json metadata
  class Zig

    attr_accessor :meta, :md

    # Init
    def initialize(meta={}, md='')
      @meta = meta; @md = md
    end

    # Load from file
    def self.load(filename)
      m, md = File.read(filename).split('+++')
      Zig.new(JSON.parse(m), md)
    end

    # Save to file
    def save(filename)
      File.write(filename, "#{@meta.to_json} +++\n#{@md}")
    end

    # Convert from hash
    def self.from_h(h)
      Zig.new(h['meta'], h['md'])
    end

    # Convert to hash
    def to_h
      { meta: @meta, md: @md }
    end

  end


  # A Zag is a collection of Zigs
  class Zag

    attr_accessor :zigs

    # Init
    def initialize(zigs=[])
      @zigs = zigs
    end

    # Load from directory
    def self.load(dir)
      Zag.new(Dir[dir+'/*.z'].map { |f| Zig.load(f) })
    end

    # Save to directory
    def save(dir)
      Dir.mkdir(dir) if !Dir.exist?(dir)
      @zigs.each do |z|
        filename = "#{z.meta['name'] || Digest::MD5.new.hexdigest(z.to_s)}.z"
        z.save("#{dir}/#{filename}")
      end
    end

    # Convert from JSON
    def self.from_json json
      Zag.new(JSON.parse(json).map(&:from_h))
    end

    # Convert to JSON
    def to_json
      JSON.pretty_generate(@zigs.map(&:to_h))
    end

  end


  # A ZSite is a Zag along with static site content
  class ZSite

    attr_accessor :doc, :styles, :libs, :zag

    # Init
    def initialize(doc, styles=[], libs=[], zag=Zag.new)
      @doc = doc
      @styles = styles
      @libs = libs
      @zag = zag
    end

    # Load from html file
    def self.load_html(filename)
      doc = Nokogiri::HTML(File.read(filename))
      styles = doc.css('.zstyle').map { |n| n.remove; { name: n['name'], css: n.inner_html } }
      libs = doc.css('.zlib').map { |n| n.remove; { name: n['name'], js: n.inner_html } }
      zag = Zag.from_json(doc.at_css('#zag').remove.inner_html)
      return ZSite.new(doc.to_html, styles, libs, zag)
    end

    # Save to html file
    def save_html(filename)
      doc = Nokogiri::HTML(@doc)
      @styles.each do |s|
        n = Nokogiri::XML::Node.new("style", doc)
        n['name'] = s[:name]; n['class'] = 'zstyle'; n.content = s[:css] 
        doc.at_css('head') << n
      end
      @libs.each do |l|
        n = Nokogiri::XML::Node.new("script", doc)
        n['name'] = l[:name]; n['class'] = 'zlib'; n.content = l[:js] 
        doc.at_css('head') << n
      end
      zag_json = Nokogiri::XML::Node.new("script", doc)
      zag_json['id'] = 'zag'; zag_json.content = zag.to_json
      doc.at_css('html') << zag_json
      File.write(filename, doc.to_html)
    end

    # Load from directory
    def self.load_dir(dir)
      doc = File.read(dir+'/index.html')
      styles = Dir[dir+'/styles/*.css'].map { |s| { name: File.basename(s, '*.css'), css: File.read(s) } }
      libs = Dir[dir+'/libs/*.js'].map { |l| { name: File.basename(l, '*.js'), js: File.read(l) } }
      zag = Zag.load(dir+'/zag')
      return ZSite.new(doc, styles, libs, zag)
    end

    # Save to directory
    def save_dir(dir)
      [dir, dir+'/styles', dir+'/libs'].each { |d| Dir.mkdir(d) if !Dir.exist?(d) }
      File.write(dir+'/index.html', @doc)
      @styles.each { |s| File.write(dir+"/styles/#{s[:name]}", s[:css]) }
      @libs.each { |l| File.write(dir+"/libs/#{l[:name]}", l[:js]) }
      @zag.save(dir+'/zag')
    end

  end

  
end